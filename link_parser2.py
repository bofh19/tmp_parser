# if you use main(url,universal_limit) as the main function
#  use  
#		http://jsonviewer.stack.hu/ 
#  to view the output out.json file properly 
#  	
#  cat out.json | python -mjson.tool > pretty_out.json 
#		for pretty output for pretty output
# 
# a sample out1.json is included which was result of running this script on www.google.com at level 5
# 

#  if you use main2(url,universal_limit) as the main function.
#  what the function does is written in readme



import urllib2
import urlparse
import re
from bs4 import BeautifulSoup as bsx
import threading
import signal
import sys
import os
import json
import itertools
import logging

BROWSER_HEADERS = [('User-agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20120427 Firefox/15.0a1')]


#root_file_name = "index.html"

#print "----- Make Sure You Are Running This In a SEPERATE FOLDER \nim not responsible if your current directory ends up with crap-----\n"


class GetURLParser:
	# class variables 
	#	self.url
	#	self.proper_domain
	#	self.universe_domain
	#	self.main_domain_name
	#	self.cur_page_hrefs
	#   self.cur_page_hrefs_json

	def __init__(self,url):
		self.url=url
		self.proper_domain = 1
		self.url = self._get_proper_url(self.url)
		universe_domain_1 = urlparse.urlparse(self.url)
		if universe_domain_1.hostname != None:
			self.universe_domain = universe_domain_1.hostname
		else:
			#logging.error("Error Not Proper Domain Entered: "+url)
			print "Error Not Proper Domain Entered: "+url
			self.proper_domain=-1
		main_domain_name = self._get_main_domain_name(url)
		if(None != main_domain_name):
			self.main_domain_name = main_domain_name
		else:
			self.main_domain_name = ''
		#print main_domain_name
	#universe_page_urls = []

	def get_links(self):  
		"""
	    # main function to get the links return result gets url ready for doing stuff
	    #  input nothing /
	    # output -> array of links in the url that was used while creating the class
	    """
		if(self.proper_domain == 1):
			self.cur_page_hrefs = self._get_page_links(self.url)
		else:
			self.cur_page_hrefs = []

		return self.cur_page_hrefs

	def _get_main_domain_name(self,url):
		this_link_href = self._get_proper_url(url)
		try:
			this_link_href_urlparse = urlparse.urlparse(this_link_href)
			this_link_getreal = this_link_href_urlparse.hostname.split('.')  # getting the root or the main domain of the url
			if(len(this_link_getreal) >= 3):								# if its same as the main domain the add this also to our list of urls
				if(len(this_link_getreal[-1])==2 and len(this_link_getreal[-2])==2): #domains url google.co.in  is also our domain link 
					return this_link_getreal[-3]
				else:
					return this_link_getreal[-2]
			elif(len(this_link_getreal) == 2):
				return this_link_getreal[0]
			else:
				return None
		except Exception, e:
			return None

	def _get_page_links(self,url): #sub function to get links
		universe_domain = self.universe_domain
		cur_page_hrefs = []	
		cur_page_hrefs_valid=[]
		
		try:
			cur_page_sx = bsx(self._get_page_data(url)) #using beautiful soup on page data
		except Exception, e:
			return []

		for each_link in cur_page_sx.find_all('a'):  #getting all links 
			this_link_href = each_link.get('href') 
			urlx = self._get_cur_url_validity(this_link_href) # caling validity on each url before adding them to the array
			if(urlx != None):
				cur_page_hrefs.append(urlx)
		
		return cur_page_hrefs

	def _get_page_data(self,url): #gets page data for the given urls
		opener = urllib2.build_opener()
		opener.addheaders = BROWSER_HEADERS
		try:
			cur_page = opener.open(url)  
			cur_page_x = cur_page.read()
			return cur_page_x
		except Exception, e:
			print e
			print url
			raise e

	def _get_cur_url_validity(self,this_link_href):  
		# checks if the current url meets our needs ..
		# like the current url is part of the main doamin (or given site ) 
		# or subdomain of the given site or is it a third party (or an external) link
		# print this_link_href	
		try: 
			this_link_href_urlparse = urlparse.urlparse(this_link_href)
			if(this_link_href_urlparse.hostname != None): # if no hostname in href then the url points to current site
				if (self._check_third_party_url(this_link_href)) :
					return this_link_href
				else:
					#logging.info("ignoring third party url "+this_link_href )
					print "ignoring third party url "+this_link_href
					return None
			else:
				if(this_link_href[0] != '/'):  # problems occur near # fragments
					this_link_href = '/'+this_link_href
				return urlparse.urljoin('http://'+self.universe_domain,this_link_href)
		except Exception, e:
			return None

	def _check_third_party_url(self,url): 
	 #the function which checks if the url is third party url or not
		this_link_href = url
		try:
			this_link_main_domain = self._get_main_domain_name(url)
			if(this_link_main_domain != None and this_link_main_domain == self.main_domain_name):
				return True
			else:
				return False
		except Exception, e:
			return False

	def _get_proper_url(self,url):
		if(url[0] == '/' and url[1] == '/'):
			url = 'http:'+url
		elif re.search('http://',url) == None and re.search('https://',url) == None:
			url = 'http://'+url
		return url


# takes url and returns array of urls in that url
def get_links(url): 
	x=GetURLParser(url)
	return x.get_links()

# returns the array of urls given in the format of  [ url, parent_id, result_id ]  
# parent_id-> is array position number, 
# result_id -> -1 or 1 (1 if data for it already fetched or -1 if its new )
def get_req_array(array,parent_id,result_calculated_id):
	result = []
	for each_array_value in array:
		result.append([each_array_value,parent_id,result_calculated_id])
	return result


# TODO - build json out of this and write it to file


def getitems(array,pid):
	x=[]
	for index,item in enumerate(array):
		if item[1]==pid:
			x.append(array[index])
	return x

def get_max(array):
	return array[len(array)-1][1]
def get_no_root_parents(array):
	for index,item in enumerate(array):
		if item[1] != -1:
			return index

def get_json(array):
	# this is supposed to work  but its not :(
	# main_json = {} 
	# d={}
	# w={}
	# for node,pid,_ in array:
	# 	print pid
	# 	if pid==-1:
	# 		w[node]=d[node]={}
	# 	else:
	# 		print "in else"
	# 		parent = array[pid][0]
	# 		w[node] = w[parent][node] = {}
	# return d
	x=[]
	for index,(item,pid,_) in enumerate(array):
		x.append([index,pid,item])
		
	for no in reversed(range(0,get_max(x)+1)):
		x[no].append(getitems(x,no))

	xxx = x[0:get_no_root_parents(x)]
	return xxx



### Old main function that produces out.json file. recursive way. using ctrl+c spoils the program output
def main():
	url = raw_input('data to get from url? -> ')
	universal_limit = raw_input('how deep to go (just press enter to end after 5 levels) -> ')

	if(universal_limit==''):
		universal_limit=5
	try:
		universal_limit = int(universal_limit)
	except Exception, e:
		# print "errorx1"
		print e
		sys.exit(0)
	x = GetURLParser(url)
	x.get_links()
	# level1_uris = x.cur_page_hrefs
	universe_counter = 1
	loop_counter = 0
	link_counter = 0
	counters = [universe_counter,loop_counter,link_counter,universal_limit]

	main_json = get_json_data(x.cur_page_hrefs_json,counters)


	f = open('out.json','w')
	json.dump(main_json,f)


# new main function that stores data in array and later simulates partial json . < that part sill not over >
def write_to_file(array):
	xxx = get_json(array)
	f = open('out.txt','w')
	json.dump(xxx,f)
	f.close()
	print """ data written to out.txt \n copy data from file. and load in ipython (%paste a) \n use \n import ast \n x=ast.literal_eval(a[0]) \n now x[0] gives you the values of its url and all its children . each child will be in the same format as x[0] if it has any children too"""

def main2(url,universal_limit):
	level_counter = 1
	link_counter = 0
	main_array = get_req_array(get_links(url),-1,-1)
	def signal_handler(*args):
		print "recieved ctrl+c. calculating json and writing to file out.txt"
		write_to_file(main_array)
		sys.exit(0)
	signal.signal(signal.SIGINT,signal_handler)

	while (level_counter<universal_limit):
		level_counter = level_counter + 1
		print "level counter-> "+str(level_counter)
		print "ulr counter -> " + str(len(main_array))
		array_counter = 0
		for each_item in main_array:
			if(each_item[2] == -1):
				main_array = main_array + get_req_array(get_links(each_item[0]),array_counter,-1)
				each_item[2]=1
			array_counter = array_counter+1

	write_to_file(main_array)
	return main_array	
	#return get_json(main_array)


if __name__ == "__main__":
	#logging.basicConfig(file_name = 'link_parser.log',level=logging.WARNING,format='%(asctime)s %(levelname)s:%(message)s',datefmt='%m/%d/%Y %I:%M:%S %p')
	url = raw_input('data to get from url? -> ')
	universal_limit = raw_input('how deep to go (just press enter to end after 5 levels) -> ')

	if(universal_limit==''):
		universal_limit=5
	try:
		universal_limit = int(universal_limit)
	except Exception, e:
		print e
		sys.exit(0)
	#print "error_log @ link_parser.log"
	main2(url,universal_limit)
