#
#  use  
#		http://jsonviewer.stack.hu/ 
#  to view the output out.json file properly 
#  	
#  cat out.json | python -mjson.tool > pretty_out.json 
#		for pretty output for pretty output
# 
# a sample out1.json is included which was result of running this script on www.google.com at level 5
# 

import urllib2
import urlparse
import re
from bs4 import BeautifulSoup as bsx
import threading
import signal
import sys
import os
import json

BROWSER_HEADERS = [('User-agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20120427 Firefox/15.0a1')]


root_file_name = "index.html"

#print "----- Make Sure You Are Running This In a SEPERATE FOLDER \nim not responsible if your current directory ends up with crap-----\n"
url = raw_input('data to get from url? -> ')
universal_limit = raw_input('how deep to go (just press enter to end after 5 levels) -> ')

if(universal_limit==''):
	universal_limit=5


class GetURLParser:
	def __init__(self,url):
		self.url=url
		self.proper_domain = 1
		if re.search('http://',self.url) == None:
			self.url = 'http://'+self.url

		universe_domain_1 = urlparse.urlparse(self.url)
		if universe_domain_1.hostname != None:
			self.universe_domain = universe_domain_1.hostname
		else:
			print "Error Not Proper Domain Entered: "+url
			self.proper_domain=-1

	universe_page_urls = []

	def get_links(self):
		if(self.proper_domain == 1):
			self.cur_page_hrefs = self.get_page_links(self.url)
			self.cur_page_hrefs_json = {}
			for each_link in self.cur_page_hrefs:
				self.cur_page_hrefs_json[each_link]=-100    #by default all urls are json objects with url as key and -100 
															#as its value. which will be changed when a function is 
															#called on that key-url. 
															#the -100 will be replaced by another big json 
															#object just like this one
		else:
			self.cur_page_hrefs = []
			self.cur_page_hrefs_json = []

	def get_page_links(self,url):
		universe_domain = self.universe_domain
		cur_page_hrefs = []	
		cur_page_hrefs_valid=[]
		opener = urllib2.build_opener()
		opener.addheaders = BROWSER_HEADERS
		try:
			cur_page = opener.open(url)  
		except Exception, e:
			print e
			return []
			
		cur_page_x = cur_page.read()
		cur_page_sx = bsx(cur_page_x)

		for each_link in cur_page_sx.find_all('a'):
			this_link_href = each_link.get('href')
			try:
				this_link_href_urlparse = urlparse.urlparse(this_link_href)

				if(this_link_href_urlparse.hostname != None): # if no hostname in href then the url points to current site
					if(this_link_href_urlparse.hostname == universe_domain):
						if re.search('http://',this_link_href) == None:   # struct the url
							this_link_href = 'http://'+this_link_href
						cur_page_hrefs.append(this_link_href)         
					else:
						getreal = universe_domain.split('.')   
						if(len(getreal) >=3):
							main_domain_name = getreal[-2]  #sub-domain like calender.google.com links when google.com is parent url is not a thrid party url.
						elif(len(getreal) == 2):                # the part from line 76 to 83 can be kept out side of this function . 
							main_domain_name = getreal[0]
						else:
							print "WTF up with the domain name "+universe_domain
							main_domain_name = ''

						this_link_getreal = this_link_href_urlparse.hostname.split('.')  # getting the root or the main domain of the url
						if(len(this_link_getreal) >= 3):								# if its same as the main domain the add this also to our list of urls
							if(this_link_getreal[-2] == main_domain_name):
								cur_page_hrefs.append(this_link_href)
							else:
								print "third party url ignoring "+this_link_href
						elif(len(this_link_getreal) == 2):
							if(this_link_getreal[0] == main_domain_name):
								cur_page_hrefs.append(this_link_href)
							else:
								print "third party url ignoring "+this_link_href
						else:
							print "third party url ignoring "+this_link_href   #if none of the conditions satisfy ignore everything
				else:
					if(this_link_href[0] != '/'):
						this_link_href = '/'+this_link_href

					cur_page_hrefs.append('http://'+universe_domain+this_link_href)

			except Exception, e:
				print e
		return cur_page_hrefs



#root_page_hrefs = get_page_links(url)

universe_counter = 1
loop_counter = 0
link_counter = 0

def get_json_data(json_array_object):
	global universe_counter
	global loop_counter
	global link_counter
	link_counter = link_counter + len(json_array_object)
	print "level counter - " + str(universe_counter)
	print "loop counter - "+str(loop_counter)
	print "link counter - " +str(link_counter)
	loop_counter += 1
	if(universe_counter >= universal_limit):
		print "-------greater "+str(universe_counter)+str(universal_limit)
		if len(json_array_object) > 0:
			for each_key in json_array_object.keys():
				x=GetURLParser(each_key)
				x.get_links()
				return x.cur_page_hrefs_json
		else:
			return {}
	else:
		if(len(json_array_object)>0):
			for each_key in json_array_object.keys():
				x = GetURLParser(each_key)
				x.get_links()
				json_array_object[each_key] = get_json_data(x.cur_page_hrefs_json)  # calling function recursively reason why i cannot save data untill all calls end.
		
		universe_counter += 1
#		print "--------------" + str(universe_counter)
		return json_array_object


try:
	universal_limit = int(universal_limit)
except Exception, e:
	print e
	sys.exit(0)



x = GetURLParser(url)
x.get_links()
# level1_uris = x.cur_page_hrefs
main_json = get_json_data(x.cur_page_hrefs_json)


f = open('out.json','w')
json.dump(main_json,f)
