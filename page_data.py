import urllib2
import urlparse
import re
from bs4 import BeautifulSoup as bsx
import threading
import signal
import sys
import os
images_types = ['JPG','jpg',
				'GIF','gif',
				'PNG','png']
linked_data_types = ['script','SCRIPT',
                     'LINK','link',
                     'IMG','img']
linked_data_types_src = {							
						'script':['src'],
                        'SCRIPT':['src'],
                        'LINK':['href'],
                        'link':['href'],
                        'IMG':['src','dfr-src'],
                        'img':['src','dfr-src'],
                        }

BROWSER_HEADERS = [('User-agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20120427 Firefox/15.0a1')]

#universe_page_urls = []

def get_page_data(url,universe_domain,root_file_name):
	try:
		opener = urllib2.build_opener()
		opener.addheaders = BROWSER_HEADERS
		cur_page = opener.open(url)  
		                 
	except Exception, e:
		print "Error Occured -> "
		print e
		return -1

	try:
		if(cur_page.code==200):
			cur_page_x = cur_page.read()
			main_file = open(root_file_name+'.tmp','w')
			main_file.write(cur_page_x)
			main_file.close() 
			cur_page_sx = bsx(cur_page_x)
		else:
			print "page error status "+cur_page.code
	except Exception, e:
		print "Error Occured2 -> "
		print e
		return -1

	cur_page_data_srcs = []

	cur_page_hrefs = []

	for each_a in cur_page_sx.find_all('a'):
		if(each_a.get('href') != None):
			cur_page_hrefs.append(each_a.get('href'))
		if(each_a.get('src') != None):
			cur_page_data_srcs.append(each_a.get('src'))



	########## getting page data css/js/images 

	#getting urls of all files
	for each_ldt in linked_data_types:
		for each_each_ldt in cur_page_sx.find_all(each_ldt):
			for each_src_type in linked_data_types_src.get(each_ldt):
				#print each_src_type +" "+str(each_each_ldt.get(each_src_type))
				if(each_each_ldt.get(each_src_type) != None):
					cur_page_data_srcs.append(each_each_ldt.get(each_src_type))

	# links validation and changing external or sub-doamins to our local doamin
	cur_page_data_srcs_valid = []
	for each_pdts in cur_page_data_srcs:
		if(urlparse.urlparse(each_pdts).hostname == None):
			if(each_pdts[0] != '/'):
				each_pdts = '/'+each_pdts
			cur_page_data_srcs_valid.append('http://'+universe_domain+each_pdts)
		else:
			if(urlparse.urlparse(each_pdts).hostname == universe_domain):
				if re.search('http://',each_pdts) == None:
					each_pdts = 'http://'+each_pdts
				cur_page_data_srcs_valid.append(each_pdts)
			else:
				## data source is from external so modify root file link external data source 
				## to our local data source file
				with open(root_file_name+'.tmp2', "wt") as out:
					for line in open(root_file_name+'.tmp'):
						line = line.replace('http://', '')
						print line
						line = line.replace(urlparse.urlparse(each_pdts).hostname,'')
						out.write(line.replace('http://', ''))
				os.remove(root_file_name+'.tmp')
				os.rename(root_file_name+'.tmp2',root_file_name+'.tmp')
				cur_page_data_srcs_valid.append(each_pdts)
				# root_file_tmp = open(root_file_name+'.tmp','r')
				# root_file_tmp2 = open(root_file_name+'.tmp2','w')
				# root_file.close()
				# root_file_tmp.close()

		

	for each_pdts_valid in cur_page_data_srcs_valid:
		o = urlparse.urlparse(each_pdts_valid)
		try:
			required_directory = '.'+o.path.rsplit('/',1)[0]
			required_file_name = o.path.rsplit('/',1)[1]
			
			if not (os.path.exists(required_directory+'/'+required_file_name)):
			
				if not os.path.exists(required_directory):
					os.makedirs(required_directory)
			
				data_file = open(required_directory+'/'+required_file_name,'w')
				opener = urllib2.build_opener()
				opener.addheaders = BROWSER_HEADERS

				try:
					cur_data = opener.open(each_pdts_valid)
					data_file.write(opener.open(each_pdts_valid).read())
					data_file.close()
					print "wrote new file "+data_file.name
				except Exception, e:
					print "skipping url " + each_pdts_valid
					print "reason : "+e
				
		except Exception, e:
			print "skipping url " + each_pdts_valid
			print "reason : "
			print e
		
	os.rename(root_file_name+'.tmp',root_file_name)